# Flask Development Assessment

1. Update the React front-end app to render a table
   containing the data returned from the Flask backend
   `/api/data` endpoint

blue_bird/__init__.py             23      1    96%

blue_bird/app.py                   6      6     0%

blue_bird/config.py                7      0   100%

blue_bird/environment.py          11      3    73%

blue_bird/routes/__init__.py       0      0   100%

blue_bird/routes/api.py           42      0   100%

blue_bird/routes/common.py        22      3    86%

blue_bird/routes/default.py       14      0   100%


TOTAL                            125     13    90%


$ % curl http://localhost:5000/api/db  
[{"color":"banana","name":"yellow","value":123},{"color":"apple","name":"red","value":234},{"color":"pumpkin","name":"orange","value":999}]
