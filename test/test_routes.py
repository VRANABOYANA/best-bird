import json
import jsonschema
import pytest

from blue_bird.routes.default import VERSION_SCHEMA
from blue_bird.routes.api import DATA_LIST_SCHEMA, DB_DATA_LIST_SCHEMA


@pytest.mark.parametrize(
    'endpoint',
    ['version', 'api/data'])
def test_bad_accept(endpoint, client):
    rv = client.post(
        endpoint,
        headers={'Accept': ['text/html']})
    assert rv.status_code == 406


def test_version_request(client):

    rv = client.post(
        'version',
        headers={'Accept': ['application/json']})
    assert rv.status_code == 200
    result = json.loads(rv.data.decode('utf-8'))
    jsonschema.validate(result, VERSION_SCHEMA)


def test_data(client):

    rv = client.post(
        'api/data',
        headers={'Accept': ['application/json']})
    assert rv.status_code == 200
    result = json.loads(rv.data.decode('utf-8'))
    jsonschema.validate(result, DATA_LIST_SCHEMA)


def test_db_data(client, dbcall):
    rv = client.post(
        'api/db',
        headers={'Accept': ['application/json']})
    assert rv.status_code == 200
    result = json.loads(rv.data.decode('utf-8'))
    jsonschema.validate(result, DB_DATA_LIST_SCHEMA)
