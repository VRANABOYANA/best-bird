import json
import os
import sqlite3
import sys
import tempfile

import pytest

import blue_bird


@pytest.fixture
def data_config_filename():
    test_config_data = {
        'str-param': 'test data string',
        'int-param': -47
    }
    with tempfile.NamedTemporaryFile() as f:
        f.write(json.dumps(test_config_data).encode('utf-8'))
        f.flush()
        yield f.name


@pytest.fixture
def client(data_config_filename):
    os.environ['SETTINGS_FILENAME'] = data_config_filename
    with blue_bird.create_app().test_client() as c:
        yield c


@pytest.fixture()
def dbcall(monkeypatch):
    db_uri = os.path.join(sys.path[1], 'data.sqlite3')
    with sqlite3.connect(db_uri) as conn:
        yield conn
