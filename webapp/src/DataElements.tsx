import React from 'react';
import Table from 'react-bootstrap/Table';
import "./styles.scss"

const DataElements: React.FC = () => {

    return <Table striped bordered hover>
        <thead>
            <th>HELLO</th>
            <th>WORLD</th>
            <th>#</th>
        </thead>
        <tbody>
            <tr>
                <td>hello</td>
                <td>world</td>
                <td>1</td>
            </tr>
            <tr>
                <td>hello</td>
                <td>world</td>
                <td>2</td>
            </tr>
            <tr>
                <td>hello</td>
                <td>world</td>
                <td>3</td>
            </tr>
        </tbody>
    </Table>;
};

export default DataElements;
