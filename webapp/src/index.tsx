import React from "react";
import ReactDOM from "react-dom";
import DataElements from "./DataElements"

ReactDOM.render(
  <DataElements />,
  document.getElementById("root") as HTMLElement);
