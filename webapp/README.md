# Working with this Web App

## Development

From this folder, run:

```bash
$ npm install
```

To run the webpack development server:

```bash
$ npm run start
```

## Releasing

To build a new bundle, run:

```bash
$ npm run build
```

This will build the new bundle and deploy it to
`blue_bird/static/*`.  This should be committed.
