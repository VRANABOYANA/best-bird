"""
API Endpoints
=========================

.. contents:: :local:

/api/data
---------------------

.. autofunction:: blue_bird.routes.api.data

"""
import binascii
import hashlib
import random
import time

from flask import Blueprint, jsonify
from blue_bird.routes import common
import sqlite3 as sqlite3

routes = Blueprint("blue-bird-api", __name__)


DATA_LIST_SCHEMA = {
    '$schema': 'http://json-schema.org/draft-07/schema#',

    'definitions': {
        'element': {
            'type': 'object',
            'properties': {
                'id': {'type': 'string'},
                'time': {'type': 'number'},
                'state': {'type': 'boolean'},
                'data1': {'type': 'string'},
                'data2': {'type': 'string'},
                'data3': {'type': 'string'}
            },
            'required': ['id', 'time', 'state', 'data1', 'data2', 'data3'],
            'additionalProperties': False
        }
    },

    'type': 'array',
    'items': {'$ref': '#/definitions/element'}
}

DB_DATA_LIST_SCHEMA = {
    "$schema": "http://json-schema.org/draft-04/schema#",

    "definitions": {
        "element": {
            "type": "object",
            "properties": {
                "color": {"type": "string"},
                "name": {"type": "string"},
                "value": {"type": "integer"}
            }
        }
    },
    "type": "array",
    "required": ["color", "name", "value"]
}


@routes.after_request
def after_request(resp):
    return common.after_request(resp)


@routes.route("/data", methods=['GET', 'POST'])
@common.require_accepts_json
def data():
    """
    handler for /api/data requests

    response will be formatted as:

    .. asjson::
        blue_bird.routes.api.DATA_LIST_SCHEMA

    :return:
    """

    def _hash(s, length):
        m = hashlib.sha256()
        m.update(s.encode('utf-8'))
        digest = binascii.b2a_hex(m.digest()).decode('utf-8')
        return digest[-length:].upper()

    def _make_element(idx):
        six_months = 24 * 3600 * 180
        return {
            'id': _hash(f'id-{idx}', 4),
            'time': int(time.time() + random.randint(-six_months, six_months)),
            'state': random.choice([True, False]),
            'data1': _hash(f'data1-{idx}', 2),
            'data2': _hash(f'data2-{idx}', 8),
            'data3': _hash(f'data3-{idx}', 32)
        }

    response = map(_make_element, range(20))
    return jsonify(list(response))


@routes.route("/db", methods=['GET', 'POST'])
@common.require_accepts_json
def db_data():
    """
    handler for /api/db requests

    response will be formatted as:

    .. asjson::
        blue_bird.routes.api.DB_DATA_LIST_SCHEMA

    :return:
    """
    data = jsonify(list(get_geant_data()))
    print(data)
    return data


def connect_to_db():
    conn = sqlite3.connect('data.sqlite3')
    return conn


def get_geant_data():
    """ gets geant data and converts to json """

    def _make_element(row):
        return {
            'color': row[0],
            'name': row[1],
            'value': int(row[2])
        }

    with connect_to_db() as conn:
        cursor = conn.cursor()
        rv = cursor.execute("SELECT * FROM geant").fetchall()
    return map(_make_element, rv)
