"""
automatically invoked app factory
"""
import logging
import os

from flask import Flask
from flask_cors import CORS  # for debugging

from blue_bird import environment
from blue_bird import config


def create_app():
    """
    overrides default settings with those found
    in the file read from env var SETTINGS_FILENAME

    :return: a new flask app instance
    """

    if 'SETTINGS_FILENAME' in os.environ:
        settings_filename = os.environ['SETTINGS_FILENAME']
    else:
        settings_filename = os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            '..', 'config-example.json'))

    with open(settings_filename) as f:
        app_config = config.load(f)

    app = Flask(__name__)
    CORS(app)

    app.secret_key = 'super secret session key'
    app.config['CONFIG_PARAMS'] = app_config

    from blue_bird.routes import default
    app.register_blueprint(default.routes, url_prefix='/')

    from blue_bird.routes import api
    app.register_blueprint(api.routes, url_prefix='/api')

    logging.info('Flask app initialized')

    environment.setup_logging()

    return app
