"""
default app creation
"""
import blue_bird
from blue_bird import environment

environment.setup_logging()

app = blue_bird.create_app()

if __name__ == "__main__":
    app.run(host="::", port="33333")
