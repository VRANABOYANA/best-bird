from setuptools import setup, find_packages

setup(
    name='blue-bird',
    version="0.1",
    author='GEANT',
    author_email='swd@geant.org',
    description='skeleton flask react app',
    url=('TBD'),
    packages=find_packages(),
    install_requires=[
        'jsonschema',
        'flask',
        'flask-cors'
    ],
    include_package_data=True,
)
